﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ファイルFToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileNewMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileOpenMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileSaveMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileSaveAsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.FileExitMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.MainTextBox = New System.Windows.Forms.TextBox()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ファイルFToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(359, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ファイルFToolStripMenuItem
        '
        Me.ファイルFToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileNewMenuItem, Me.FileOpenMenuItem, Me.FileSaveMenuItem, Me.FileSaveAsMenuItem, Me.ToolStripSeparator1, Me.FileExitMenuItem})
        Me.ファイルFToolStripMenuItem.Name = "ファイルFToolStripMenuItem"
        Me.ファイルFToolStripMenuItem.Size = New System.Drawing.Size(70, 20)
        Me.ファイルFToolStripMenuItem.Text = "ファイル(&F)"
        '
        'FileNewMenuItem
        '
        Me.FileNewMenuItem.Image = Global.P161A_TextEditor1.My.Resources.Resources.NewDocument
        Me.FileNewMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.FileNewMenuItem.Name = "FileNewMenuItem"
        Me.FileNewMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.FileNewMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.FileNewMenuItem.Text = "新規作成(&N)"
        '
        'FileOpenMenuItem
        '
        Me.FileOpenMenuItem.Image = Global.P161A_TextEditor1.My.Resources.Resources.OpenFolder2
        Me.FileOpenMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.FileOpenMenuItem.Name = "FileOpenMenuItem"
        Me.FileOpenMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.FileOpenMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.FileOpenMenuItem.Text = "開く(&O)"
        '
        'FileSaveMenuItem
        '
        Me.FileSaveMenuItem.Image = Global.P161A_TextEditor1.My.Resources.Resources.Save
        Me.FileSaveMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.FileSaveMenuItem.Name = "FileSaveMenuItem"
        Me.FileSaveMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.FileSaveMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.FileSaveMenuItem.Text = "上書き保存(&S)"
        '
        'FileSaveAsMenuItem
        '
        Me.FileSaveAsMenuItem.Name = "FileSaveAsMenuItem"
        Me.FileSaveAsMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.FileSaveAsMenuItem.Text = "名前を付けて保存(&A)..."
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(192, 6)
        '
        'FileExitMenuItem
        '
        Me.FileExitMenuItem.Name = "FileExitMenuItem"
        Me.FileExitMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.FileExitMenuItem.Text = "終了(&X)"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.AutoUpgradeEnabled = False
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.ShowReadOnly = True
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.AutoUpgradeEnabled = False
        '
        'MainTextBox
        '
        Me.MainTextBox.AcceptsReturn = True
        Me.MainTextBox.AcceptsTab = True
        Me.MainTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainTextBox.Location = New System.Drawing.Point(0, 24)
        Me.MainTextBox.MaxLength = 0
        Me.MainTextBox.Multiline = True
        Me.MainTextBox.Name = "MainTextBox"
        Me.MainTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.MainTextBox.Size = New System.Drawing.Size(359, 217)
        Me.MainTextBox.TabIndex = 1
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(359, 241)
        Me.Controls.Add(Me.MainTextBox)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(200, 150)
        Me.Name = "MainForm"
        Me.Text = "MainForm"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ファイルFToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents FileNewMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileOpenMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileSaveMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileSaveAsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents FileExitMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MainTextBox As System.Windows.Forms.TextBox

End Class
