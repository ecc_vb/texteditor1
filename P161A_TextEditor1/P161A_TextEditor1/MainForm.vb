﻿Imports System.IO, System.Text

' Project:P161A_TextEditor1
' Auther:IE2A No.24, 村田直人
' Date: 2015年07月31日

Public Class MainForm

    Private ReadOnly AppliTitle As String = My.Application.Info.Title


    'Formロードメソッド
    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'フォームサイズ設定
        With My.Computer.Screen.Bounds
            Me.Size = New Size(.Width \ 2, .Height * 2 \ 3)
        End With

        'テキストボックスのフォント設定
        With MainTextBox
            .Font = New Font("MS ゴシック", 10.5F)
            .WordWrap = False '右端折り返し
        End With

        'ファイルを開く、ファイル保存ダイアログのファイルフィルタ
        With OpenFileDialog1
            .Filter = _
                "テキストファイル(*.txt)|*.txt" & _
                "HTML ファイル(*.htm;*html)|*.htm;*.html|" & _
                "XML ファイル(*.xml)|*.xml|" & _
                "VB ファイル(*.vb)|*.vb|" & _
                "全てのファイル(*.*) |*.*"
            SaveFileDialog1.Filter = .Filter
        End With

    End Sub

    '新規作成メニュー、フォームロード
    Private Sub FileNewMenuItem_Click(sender As Object, e As EventArgs) Handles FileNewMenuItem.Click, MyBase.Load

        'テキストボックスのプロパティ設定
        With MainTextBox
            .Clear()
            .ReadOnly = False
            .Select(0, 0)
        End With

        'フォームタイトルバーの表示
        Me.Text = "無題 -" & AppliTitle

        'ダイアログのファイル名クリア
        OpenFileDialog1.FileName = ""
        SaveFileDialog1.FileName = ""

    End Sub

    '開くメニュー
    Private Sub FileOpenMenuItem_Click(sender As Object, e As EventArgs) Handles FileOpenMenuItem.Click

        With OpenFileDialog1

            .ReadOnlyChecked = False '読み取り専用チェックをオフに

            If .ShowDialog = DialogResult.Cancel Then
                Exit Sub 'キャンセルされた時プロシージャを終了
            End If

            Try
                '選択したファイルを表示
                MainTextBox.Text = File.ReadAllText(.FileName, Encoding.Default)
            Catch ex As Exception
                '例外が発生した場合メッセージを表示してプロシージャを終了
                MessageBox.Show _
                    (ex.Message, AppliTitle, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try

            MainTextBox.ReadOnly = .ReadOnlyChecked '読み取り専用On or Off設定
            MainTextBox.Select(0, 0) 'カーソル,選択文字数設定

            Me.Text = Path.GetFileName(.FileName) & " - " & AppliTitle 'titleにファイル名表示

            SaveFileDialog1.FileName = .FileName '名前を付けて保存


        End With


        

    End Sub

    Private Sub FileSaveMenuItem_Click(sender As Object, e As EventArgs) Handles FileSaveMenuItem.Click


        With OpenFileDialog1

            If .FileName = "" OrElse MainTextBox.ReadOnly Then '新規作成orReadOnryの場合

                FileSaveMenuItem.PerformClick() '「名(前を付けて保存」のクリックイベント呼び出し
                Exit Sub 'プロシージャ終了

            End If

            SaveFile(.FileName)

        End With

    End Sub


    '「名前を付けて保存」メニュー
    Private Sub FileSaveAsMenuItem_Click(sender As Object, e As EventArgs) Handles FileSaveAsMenuItem.Click

        With SaveFileDialog1

            If .ShowDialog = DialogResult.Cancel Then 'ダイアログでキャンセルが押された場合

                Exit Sub 'プロシージャ終了
            End If

            'ファイル保存メソッド
            If SaveFile(.FileName) = False Then '保存出来なかった場合

                .FileName = OpenFileDialog1.FileName 'ファイルパスを元に戻す
                Exit Sub 'プロシージャ終了

            End If

            MainTextBox.ReadOnly = False '読み取り専用解除
            Me.Text = Path.GetFileName(.FileName) & " - " & AppliTitle 'titleにファイル名表示
            OpenFileDialog1.FileName = .FileName 'ファイルパス更新

        End With

    End Sub


    Private Sub FileExitMenuItem_Click(sender As Object, e As EventArgs) Handles FileExitMenuItem.Click
        Me.Close() 'フォームを閉じる
    End Sub

    Private Function SaveFile(ByVal fName As String) As Boolean

        Try
            File.WriteAllText(fName, MainTextBox.Text, Encoding.Default) 'ファイルへの書き込み

            SaveFile = True '戻り値設定
        Catch ex As Exception

            MessageBox.Show _
                (ex.Message, AppliTitle, MessageBoxButtons.OK, MessageBoxIcon.Error) 'エラー表示
            SaveFile = False '戻り値設定

        End Try

    End Function

End Class
